# Gos Booking System

Follow the below steps to setup project environment
-------------------------------------------------------------------
1. Make sure to have installed python3 on your local system
2. Install pip using --> sudo apt-get install python3-pip
3. install virtual environment using --> sudo pip3 install virtualenv 
4. create virtual environment --> virtualenv gasbooking_env
5. Activate the above created virtualenv --> source gasbooking_env/bin/activate
6. install requirements.txt using --> pip install -r requirements.txt
9. Migrate the model changes to database -->  run command python manage.py migrate
10. Run the development server using --> python manage.py runserver
